class Post < ActiveRecord::Base
    has_many :comments, dependent: :destroy
    # post should have a title and body to be added to db
    # validates_presence_of :title
    # validates_presence_of :body
    validates :title, presence: true
    validates :body, presence: true
end
